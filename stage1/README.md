# Stage 1 PKGBUILD'S

This folder contains the patched `PKGBUILD`s for natively building in the "dirty" Buildroot target chroot. These `PKGBUILD`s will overwrite the corresponding libraries from Buildroot, and piece-by-piece, turning it into an Arch system.

The process is highly manual, but we have chosen this route as Felix Yan has suggested this way, and he had also successfully used this method for his RISC-V Arch port.