# Maintainer: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Johannes Löthberg <johannes@kyriasis.com>
# Contributor: Piotr Gorski <lucjan.lucjanov@gmail.com>
# Contributor: Lucy <lucy@luz.lu>
# Contributor: Bartlomiej Piotrowski <nospam@bpiotrowski.pl>

pkgname=pkgconf
pkgver=1.7.3
pkgrel=1
pkgdesc="Package compiler and linker metadata toolkit"
url="https://git.sr.ht/~kaniini/pkgconf"
license=(custom:ISC)
arch=(mipsisa64r6el)
depends=(glibc sh)
makedepends=(git)
provides=(pkg-config pkgconfig)
conflicts=(pkg-config)
replaces=(pkg-config)
groups=(base-devel)
_commit=aca0674837cb6df1b29faddb8afe6b2f39733f6b  # tags/pkgconf-1.7.3
source=("git+https://git.sr.ht/~kaniini/pkgconf#commit=$_commit"
        mipsisa64r6el-unknown-linux-gnuabi64.personality)
sha256sums=('SKIP'
            'df6f40c26e78bc401703e8380ce52984428f6303105ade66bf73848c4847b426')

_pcdirs=/usr/lib/pkgconfig:/usr/share/pkgconfig
_libdir=/usr/lib
_includedir=/usr/include

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/^pkgconf-//;s/-/+/g'
}

prepare() {
  mkdir build
  cd $pkgname
  ./autogen.sh
}

build() {
  cd build
  ../$pkgname/configure \
    --host=${CHOST} \
    --target=${CHOST} \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-pkg-config-dir="$_pcdirs" \
    --with-system-libdir="$_libdir" \
    --with-system-includedir="$_includedir" \
    --disable-static
  make
}

package() {
  DESTDIR="$pkgdir" make -C build install

  install -Dt "$pkgdir/usr/share/pkgconfig/personality.d" -m644 \
    mipsisa64r6el-unknown-linux-gnuabi64.personality
  ln -s pkgconf "$pkgdir/usr/bin/mipsisa64r6el-unknown-linux-gnuabi64-pkg-config"
  ln -s pkgconf "$pkgdir/usr/bin/pkg-config"

  ln -s pkgconf.1 "$pkgdir/usr/share/man/man1/pkg-config.1"
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 $pkgname/COPYING
}

# vim:set sw=2 et:
