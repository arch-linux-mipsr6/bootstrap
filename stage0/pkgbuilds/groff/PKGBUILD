# Maintainer: 

pkgname=groff
pkgver=1.22.4
pkgrel=5
pkgdesc='GNU troff text-formatting system'
arch=('mipsisa64r6el')
url='https://www.gnu.org/software/groff/groff.html'
license=('GPL')
groups=('base-devel')
depends=('perl' 'gcc-libs')
makedepends=('netpbm' 'psutils'  'libxaw' 'perl-file-homedir')
optdepends=('netpbm: for use together with man -H command interaction in browsers'
            'psutils: for use together with man -H command interaction in browsers'
            'libxaw: for gxditview'
            'perl-file-homedir: for use with glilypond')
options=('!docs' '!emptydirs' '!makeflags')
validpgpkeys=('2D0C08D2B0AD0D3D8626670272D23FBAC99D4E75') # Bertrand Garrigues <bertrand.garrigues@laposte.net>
source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.gz"{,.sig}
        'site.tmac')
md5sums=('08fb04335e2f5e73f23ea4c3adbf0c5f'
         'SKIP'
         'a1fedafd7863b37968d32ad9ae1d8c13')
sha256sums=('e78e7b4cb7dec310849004fa88847c44701e8d133b5d4c13057d876c1bad0293'
            'SKIP'
            'af59ecde597ce9f8189368a7739279a5f8a391139fe048ef6b4e493ed46e5f5f')

build() {
  cd $pkgname-$pkgver

   # disable uchardet and X11 support for now as we don't have them yet
   ./configure \
    --host=${CHOST} \
    --target=${CHOST} \
    --prefix=/usr \
    --without-uchardet \
    --without-x
   # use host groff binary to generate the docs for cross compiling
   sed -i '/GROFFBIN = $(abs_top_builddir)\/groff/cGROFFBIN = \/usr\/bin\/groff' Makefile
   sed -i '/GROFF_BIN_PATH = $(abs_top_builddir)/cGROFF_BIN_PATH = \/usr\/bin' Makefile
   make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  
  # add compatibility symlinks
  ln -s eqn "$pkgdir"/usr/bin/geqn
  ln -s tbl "$pkgdir"/usr/bin/gtbl
  ln -s soelim "$pkgdir"/usr/bin/zsoelim

  # FS33760 - TERMCAP variables not followed
  # TODO: everyone is doing this - find out why upstream does not...
  cat "$srcdir"/site.tmac >> \
    "$pkgdir"/usr/share/groff/site-tmac/man.local
  cat "$srcdir"/site.tmac >> \
    "$pkgdir"/usr/share/groff/site-tmac/mdoc.local
}
