#!/bin/bash

TRIPLET=mipsisa64r6el-unknown-linux-gnuabi64
SDK_PATH=/scratch/cross/arch-bootstrap/mipsisa64r6el-unknown-linux-gnuabi64_sdk-buildroot
PACMAN=pacman-5.2.2

echo "Preparaing pacman sources..."
ln -sf ${PACMAN} pacman
pushd ${PACMAN}
patch -Np1 < ../pacman-5.2.2-fix-strip-messing-up-file-attributes.patch
popd

rm -rf build
mkdir build

pushd build
../pacman/configure --host=${TRIPLET} --target=${TRIPLET} \
  --prefix=/usr --sysconfdir=/etc \
  --localstatedir=/var --disable-doc \
  --with-scriptlet-shell=/usr/bin/bash \
  --with-ldconfig=/usr/bin/ldconfig

echo "pacman configured, now you should run make in build directory..."
