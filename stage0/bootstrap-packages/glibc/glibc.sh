#!/bin/bash

TRIPLET=mipsisa64r6el-unknown-linux-gnuabi64
SDK_PATH=/scratch/cross/arch-bootstrap/mipsisa64r6el-unknown-linux-gnuabi64_sdk-buildroot
GLIBC=glibc-2.32

echo "Preparaing glibc sources..."
ln -sf ${GLIBC} glibc

rm -rf build
mkdir build

pushd build
echo "slibdir=/usr/lib" >> configparms
echo "rtlddir=/usr/lib" >> configparms
echo "sbindir=/usr/bin" >> configparms
echo "rootsbindir=/usr/bin" >> configparms

../glibc/configure --host=${TRIPLET} --target={TRIPLET} \
  --prefix=/usr --enable-shared --disable-profile --disable-werror \
  --enable-stack-protector=strong --enable-stackguard-randomization --enable-bind-now \
  --enable-lock-elision --enable-add-ons --enable-kernel=4.4 --without-gd --disable-multi-arch \
  --with-bugurl=https://gitlab.com/arch-linux-mipsisa64r6el/arch-linux-mipsisa64r6el-bootstrap/-/issues

echo "glibc configured, now you should run make in build directory..."
