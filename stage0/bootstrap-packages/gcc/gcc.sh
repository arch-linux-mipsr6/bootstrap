#!/bin/bash

## Prepare the GCC sources and configure GCC for the target system
## Author: Tom Bu

TRIPLET=mipsisa64r6el-unknown-linux-gnuabi64
GMP=gmp-6.1.0
ISL=isl-0.24
MPC=mpc-1.0.3
MPFR=mpfr-3.1.4
GCC=gcc-10.3.0

pushd ${GCC}

# Link source-level dependencies
ln -fs ../${GMP} gmp
ln -fs ../${ISL} isl
ln -fs ../${MPC} mpc
ln -fs ../${MPFR} mpfr

echo 10.3.0 > gcc/BASE-VER

# Patch GCC
if [ -f patched ]
then
  echo "GCC has already been patched, skipping..."
else
  echo "Patching GCC..."
  patch -p1 -i ../gdc_phobos_path.patch
  patch -p1 -i ../fs64270.patch
  sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" {libiberty,gcc}/configure
  echo "patched" > patched
fi
popd

rm -rf build
mkdir build
pushd build

../${GCC}/configure --prefix=/usr --libdir=/usr/lib  --libexecdir=/usr/lib \
  --mandir=/usr/share/man --infodir=/usr/share/info --with-native-system-header-dir=/usr/include \
  --host=${TRIPLET} --target=${TRIPLET} --with-arch=mips64r6 --with-abi=64 \
  --enable-shared --enable-threads=posix --enable-languages=c,c++,fortran,d,lto --with-system-zlib \
  --with-isl --with-linker-hash-style=both --with-system-zlib --with-default-libstdcxx-abi=new \
  --enable-linker-build-id --enable-checking=release --enable-libstdcxx-time=yes --enable-nls --enable-lto \
  --enable-__cxa_atexit --enable-install-libiberty --enable-default-pie --enable-default-ssp --enable-clocale=gnu \
  --enable-gnu-indirect-function --enable-gnu-unique-object \
  --disable-libitm --disable-libstdcxx-pch --disable-werror --disable-libssp \
  --disable-libunwind-exceptions --disable-libsanitizer --disable-libquadmath \
  --disable-libquadmath-support --disable-libphobos --disable-multilib \
  --with-bugurl=https://gitlab.com/arch-linux-mipsisa64r6el/arch-linux-mipsisa64r6el-bootstrap/-/issues

echo "GCC configured, you should now run make to build GCC in the build directory."
