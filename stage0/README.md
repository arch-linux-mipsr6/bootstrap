# Stage 0 PKGBUILD'S

This folder contains the patched `PKGBUILD`s for the `base-devl` group and some other key packages required for natively building packages. These patched versions allow cross-building using the `makepkg` program from the host and the Buildroot SDK. Some features may be disabled due to the lack of dependencies, and others contain hacks to allow the packages to build.

# Notes
- `autoconf`: re-used upstream package as its architecture is `any`.
- `automake`: re-used upstream package as its architecture is `any`.
- `binutils`: `ld.gold` is disabled as it seems to be broken and spits out template errors when compiled. `debuginfod` is also disabled as we don't have that yet.
- `fakeroot`: replaced with `fakeroot-tcp` as the former refuses to be cross-compiled.
- `flex`: disabled documentation generation with `help2man` (via a hack documented by Linux from Scratch) as it does not work when cross compiling.
- `gawk`: requires `gmp` and `mpfr`to be extracted to a temporary directory that is outside the Buildroot target system. Also remember to set the path in PKGBUILD.
- `gcc`: requires `gmp`, `mpfr` and `libmpc` to be extracted to a temporary directory that is outside the Buildroot target system. Also remember to set the path in PKGBUILD.
- `groff`: the `Makefile` has to be patched to use the `groff` binary from the host to generate documentation, instead of the newly-compiled binary, as we are cross compiling.
- `less`: uses `pcre` instead of `pcre2` as the regular expression library as we do not have the latter yet.
- `libmpc`: requires `gmp` and `mpfr`to be extracted to a temporary directory that is outside the Buildroot target system. Also remember to set the path in PKGBUILD.
- `make`: disabled Guile support as we do not have Guile yet.
- `sudo`: does not work yet as we have not rebuilt `pam`.
- `util-linux`: disabled `hardlink` functionality as it requires `pcre2` and we do not have it yet.
