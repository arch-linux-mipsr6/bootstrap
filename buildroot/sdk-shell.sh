#!/bin/bash

## A utility script to spawn a shell with the SDK environment set up
## Author: Tom Bu

## Modify to reflect your installation 
SDK_LOCATION=/scratch/cross/arch-bootstrap

echo "Relocating SDK..."
bash ${SDK_LOCATION}/mipsisa64r6el-unknown-linux-gnuabi64_sdk-buildroot/relocate-sdk.sh

## Set required environment variables
echo "Configuring environment..."
export PATH=${PATH}:${SDK_LOCATION}/mipsisa64r6el-unknown-linux-gnuabi64_sdk-buildroot/bin;
export CFLAGS="-O2 -fstack-protector-strong --param=ssp-buffer-size=4 -fstack-clash-protection -D_FORTIFY_SOURCE=2"
export CXXFLAGS="-O2 -fstack-protector-strong --param=ssp-buffer-size=4 -fstack-clash-protection -D_FORTIFY_SOURCE=2"
export LDFLAGS="-Wl,-z,relro,-z,now,-O1,--sort-common,--as-needed"

## Run a new shell with the correct setup
echo "Your SDK shell is here..."
exec ${SHELL}
