# MIPS64 r6 (little endian) port of Arch Linux
This repository contains the tooling and patches used to bootstrap Arch Linux on the MIPS r6 instruction set (non-backward-compatible). This project is possible only with the support from [CIP United](http://www.cipunited.com/) and non-bootstrap-specific patches will be upstreamed for inclusion.

## Basic information
This project aims to build a pure 64-bit Arch Linux environment with the `mipsisa64r6el-unknown-linux-gnuabi64` triplet, and uses a patched version of [Buildroot](https://buildroot.org) (with custom triplets) as a bootstrapping environment. We also referred to [parabola-cross-bootstrap](https://git.parabola.nu/parabola-cross-bootstrap.git/) and [bootstrap32](https://git.archlinux32.org/bootstrap32/) for the exact bootstrap steps as Arch does not officially provide porting cookbooks.

## Current State and Next Step
Stage 1 bootstrap is completed and self-hosting chroot environment is now ready.

Further work will be conducted at the [packages](https://gitlab.com/arch-linux-mipsr6/packages) repository.
